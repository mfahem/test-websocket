package com.example.testwebsocket;

public class ServerConfig {
    private String MessageID;
    private Integer VoipPort;
    private Integer VideoPort;
    private Integer AudioBitRate;
    private Integer AudioSampleRate;
    private Integer AudioFrameSize;

    public String getMessageID() {
        return MessageID;
    }

    public void setMessageID(String messageID) {
        MessageID = messageID;
    }

    public Integer getVoipPort() {
        return VoipPort;
    }

    public void setVoipPort(Integer voipPort) {
        VoipPort = voipPort;
    }

    public Integer getVideoPort() {
        return VideoPort;
    }

    public void setVideoPort(Integer videoPort) {
        VideoPort = videoPort;
    }

    public Integer getAudioBitRate() {
        return AudioBitRate;
    }

    public void setAudioBitRate(Integer audioBitRate) {
        AudioBitRate = audioBitRate;
    }

    public Integer getAudioSampleRate() {
        return AudioSampleRate;
    }

    public void setAudioSampleRate(Integer audioSampleRate) {
        AudioSampleRate = audioSampleRate;
    }

    public Integer getAudioFrameSize() {
        return AudioFrameSize;
    }

    public void setAudioFrameSize(Integer audioFrameSize) {
        AudioFrameSize = audioFrameSize;
    }
}
