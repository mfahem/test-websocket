package com.example.testwebsocket;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class WebSocketService {
    private final Gson gson = new GsonBuilder().create();
    public void connectToWebSocket() {
        // Create a WebSocket. The scheme part can be one of the following:
        // 'ws', 'wss', 'http' and 'https' (case-insensitive). The user info
        // part, if any, is interpreted as expected. If a raw socket failed
        // to be created, an IOException is thrown.
        WebSocket ws;
        try {
            ws = new WebSocketFactory().setConnectionTimeout(0).createSocket("ws://delta.instavoxapp.fr:10200");

            // Register a listener to receive WebSocket events.
            ws.addListener(new WebSocketAdapter() {
                @Override
                public void onTextMessage(WebSocket websocket, String message) throws Exception {
                    // Received a text message.
                    Log.d("WEB SOCKET", message);
                    ServerConfig[] serverConfigs = gson.fromJson(message, ServerConfig[].class);
                    if(serverConfigs.length > 0) {
                        String s = "[{AppName: \"TEST\", AudioCodec: 1, DeviceData: { SessionID: " + randomId() + ", ID: \"ZIUTCdKbrUW+/f5K/H7iNg==\", DeviceDescription: \"MANUFACTURER=WLLC;MODEL=APIClientWEB;SERIAL=123456789;OSVERSION=5.0\", Login: \"API\", AvatarHash: \"\", StatusID: \"AAAAAAAAAAAAAAAAAAAAAA==\" }, MessageID: \"DEVICE_CONFIG\", Password: \"aom2021\", Ssrc: 123, VersionCode: 1, VersionName: \"5.5\"}]";
                        ws.sendText(s);
                    }
                }

                @Override
                public void onTextMessageError(WebSocket websocket, WebSocketException cause, byte[] data) throws Exception {
                    super.onTextMessageError(websocket, cause, data);
                }

                @Override
                public void onError(WebSocket websocket, WebSocketException cause) throws Exception {
                    super.onError(websocket, cause);
                }

                @Override
                public void onFrameError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame) throws Exception {
                    super.onFrameError(websocket, cause, frame);
                }

                @Override
                public void onFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onFrame(websocket, frame);
                }

                @Override
                public void onFrameUnsent(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onFrameUnsent(websocket, frame);
                }

                @Override
                public void onSendingFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onSendingFrame(websocket, frame);
                }

                @Override
                public void onSendError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame) throws Exception {
                    super.onSendError(websocket, cause, frame);
                }

                @Override
                public void onFrameSent(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onFrameSent(websocket, frame);
                }

                @Override
                public void onContinuationFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onContinuationFrame(websocket, frame);
                }

                @Override
                public void onTextFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onTextFrame(websocket, frame);
                }

                @Override
                public void onTextMessage(WebSocket websocket, byte[] data) throws Exception {
                    super.onTextMessage(websocket, data);
                }

                @Override
                public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
                    super.onConnected(websocket, headers);
                    Log.d("WEB SOCKET", "Connected");
                }

                @Override
                public void onConnectError(WebSocket websocket, WebSocketException exception) throws Exception {
                    super.onConnectError(websocket, exception);

                    Log.e("WEB SOCKET", "Connetion error");
                }

                @Override
                public void onPongFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onPongFrame(websocket, frame);
                }

                @Override
                public void onSendingHandshake(WebSocket websocket, String requestLine, List<String[]> headers) throws Exception {
                    super.onSendingHandshake(websocket, requestLine, headers);
                }

                @Override
                public void onUnexpectedError(WebSocket websocket, WebSocketException cause) throws Exception {
                    super.onUnexpectedError(websocket, cause);
                }

                @Override
                public void handleCallbackError(WebSocket websocket, Throwable cause) throws Exception {
                    super.handleCallbackError(websocket, cause);
                }
            });

            ws.connectAsynchronously();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String randomId() {
        // Create random UUID
        UUID uuid = UUID.randomUUID();

        // Create byte[] for base64 from uuid
        byte[] src = ByteBuffer.wrap(new byte[16])
                .putLong(uuid.getMostSignificantBits())
                .putLong(uuid.getLeastSignificantBits())
                .array();

        // Encode to Base64 and remove trailing ==
        Base64.Encoder encoder = Base64.getUrlEncoder();
        return encoder.encodeToString(src);
    }
}
